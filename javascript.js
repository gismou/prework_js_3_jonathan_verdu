var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

function dni(numDNI, letraDNI){
    var restoDNI = numDNI % 23;
    if(restoDNI < 0 && restoDNI > 99999999) console.log("El número no es válido");
    else{
        var letraDNIObtenida = letras[restoDNI];
        if(letraDNIObtenida == letraDNI) console.log("DNI Válido");
        else console.log("DNI Incorrecto");
    }
}

var numDNI = prompt("Escribe el número de tu dni");
var letraDNI = prompt("Escribe la letra de tu dni");

dni(numDNI, letraDNI);